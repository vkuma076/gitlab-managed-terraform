variable "bucket_name" {
    default = "vin-s3-bucket"
}

variable "obj_name" {
    default = "vin-0.0.1-SNAPSHOT.jar"
}

variable "vpc_name" {
    default = "vin-default-vpc"
}

variable "ec2_name" {
    default = "vin-ec2-instance"
}
variable "sg_name_1" {
    default = "allow-TCP-8080"
}

variable "sg_name_2" {
    default = "allow-SSH-22"
}


variable "ec2_policy_name" {
    default = "vin-ec2-policy"
}

variable "ec2_role_name" {
    default = "vin-ec2-role"
}

variable "ec2_attachment" {
    default = "vin-ec2-attachment"
}

variable "ec2_profile" {
    default = "vin-ec2-profile"
}

variable "directory" {
    type = string
    default = ""
}

variable "gitlab_api_token" {
    type = string
    default = ""
}

variable "api_url" {
    type = string
    default = ""
}

variable "project_id" {
    type = string
    default = ""
}

variable "project_name" {
    type = string
    default = ""
}