terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-west-2"
}

resource "aws_s3_bucket" "aws-bucket-1" {
  bucket = var.bucket_name

  tags = {
    Name = var.bucket_name
  }
}

resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  bucket = aws_s3_bucket.aws-bucket-1.id
  policy = data.aws_iam_policy_document.allow_access_from_another_account.json
}

data "aws_iam_policy_document" "allow_access_from_another_account" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
      "s3:GetObjectVersion"
    ]

    resources = [
      aws_s3_bucket.aws-bucket-1.arn,
      "${aws_s3_bucket.aws-bucket-1.arn}/*",
    ]
  }
}

# jar name specified in pom.xml
resource "aws_s3_bucket_object" "app-jar" {
  bucket = var.bucket_name
  key    = var.obj_name
  source = "${var.directory}/target/${var.obj_name}"
  depends_on = [
    aws_s3_bucket.aws-bucket-1
  ]
}

resource "aws_instance" "app_server" {
  ami                  = "ami-08e2d37b6a0129927"
  instance_type        = "t2.micro"
  security_groups      = ["allow_tcp", "allow_ssh"]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  key_name             = "ec2-key-pair"
  user_data            = <<EOF
    #!/bin/bash
    echo "Update dependencies"
    sudo yum update -y
    echo "Install Java 11"
    sudo yum install -y java-11-amazon-corretto-headless    
    echo "Pull spring boot app from S3 bucket"
    aws s3api get-object --bucket ${var.bucket_name} --key ${var.obj_name} ${var.obj_name}
    echo "run the spring boot app"
    sudo java -jar ${var.obj_name}
    echo "spring boot app should be up and running"
    EOF
  tags = {
    Name = var.ec2_name
  }
}


resource "aws_default_vpc" "default_vpc" {
  tags = {
    Name = var.vpc_name
  }
}

resource "aws_security_group" "allow_TCP" {
  name        = "allow_tcp"
  description = "Allow TCP inbound and outbound traffic"

  ingress {
    description = "TCP from VPC"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tcp"
  }
}

resource "aws_security_group" "allow_SSH" {
  name        = "allow_ssh"
  description = "Allow SSH inbound and outbound traffic"

  ingress {
    description = "SSH from LocalVPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tcp"
  }
}



resource "aws_iam_policy" "ec2_policy" {
  name        = var.ec2_policy_name
  path        = "/"
  description = "policy to allow ec2 instance to access S3 bucket"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetObject",
          "s3:ListBucket"
        ],
        "Resource" : [
          "arn:aws:s3:::${var.bucket_name}/*",
          "arn:aws:s3:::${var.bucket_name}"
        ]
      },
      {
        "Effect" : "Allow",
        "Action" : "s3:ListAllMyBuckets",
        "Resource" : "*"
      }
    ]
  })
}

resource "aws_iam_role" "ec2_role" {
  name = var.ec2_role_name
  assume_role_policy = jsonencode({ Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_policy_attachment" "ec2_policy_role" {
  name       = var.ec2_attachment
  roles      = [aws_iam_role.ec2_role.name]
  policy_arn = aws_iam_policy.ec2_policy.arn
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = var.ec2_profile
  role = aws_iam_role.ec2_role.name
}
